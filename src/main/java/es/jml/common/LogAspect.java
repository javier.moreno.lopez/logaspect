package es.jml.common;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LogAspect {
	@Around("execution(* es.jml.web..*(..))" + " || execution(* es.jml.domain..*(..))"
			+ " || execution(* es.jml.persistence..*(..))")
	public Object logCall(ProceedingJoinPoint proceedingJointPoint) throws Throwable {
		Signature signature = proceedingJointPoint.getStaticPart().getSignature();
		String methodFullyQualifiedName = String.format("%s.%s", signature.getDeclaringTypeName(), signature.getName());
		;
		Logger logger = LogManager.getLogger(methodFullyQualifiedName);
		if (!logger.isTraceEnabled())
			return proceedingJointPoint.proceed(proceedingJointPoint.getArgs());

		Object[] args = proceedingJointPoint.getArgs();
		String argList = Arrays.toString(args);
		String invocation = String.format("%s(%s)", methodFullyQualifiedName, argList);
		logger.trace(String.format("%s started.", invocation));
		long start = System.nanoTime();
		try {
			Object proceed = proceedingJointPoint.proceed(args);
			long end = System.nanoTime();
			logger.trace(String.format("%s ended after %f ms returning: %s.", invocation,
					((double) (end - start) / 1000000), proceed));
			return proceed;
		} catch (Throwable T) {
			long end = System.nanoTime();
			logger.trace(String.format("%s throwed exception after %f ms. %s.", invocation,
					((double) (end - start) / 1000000), T));
			throw T;
		}
	}
}
